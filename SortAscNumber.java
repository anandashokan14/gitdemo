package com.stackroute.basics;

import java.lang.management.ThreadInfo;
import java.util.Scanner;

public class SortAscNumber {

    public static void main(String[] args) {
        new SortAscNumber().getNumbers();
    }

    //get the numbers from user through console
    public void getNumbers() {
    	System.out.println("Enter the Numbers: ");
		Scanner sc=new  Scanner(System.in);
		int firstNum = sc.nextInt();
		int secondNum = sc.nextInt();
		int thirdNum = sc.nextInt();
		int fourthNum = sc.nextInt();
		new SortAscNumber().numberSorter(firstNum, secondNum, thirdNum, fourthNum);
    }

    //logic to sort the numbers
    public String numberSorter(int firstNumber, int secondNumber, int thirdNumber, int fourthNumber) {
    	
    	/*
    	if (firstNumber <= secondNumber && secondNumber <= thirdNumber && thirdNumber <= fourthNumber) {
    		
    		return "Sorted:{"+firstNumber+","+secondNumber+","+thirdNumber+","+fourthNumber+"}";
    		
    	}
    	else if(firstNumber <= secondNumber && secondNumber <= fourthNumber &&  fourthNumber <= thirdNumber) {
    		
    		return "Sorted:{"+firstNumber+","+secondNumber+","+fourthNumber+","+thirdNumber+"}";
    		
    	}
    	else if(firstNumber <= thirdNumber && thirdNumber <= secondNumber &&  secondNumber <= fourthNumber) {
    		
    		return "Sorted:{"+firstNumber+","+thirdNumber+","+secondNumber+","+fourthNumber+"}";
    	
    	}
    	else if(firstNumber <= thirdNumber && thirdNumber <= fourthNumber && fourthNumber <= secondNumber) {
    		
    		return "Sorted:{"+firstNumber+","+thirdNumber+","+fourthNumber+","+secondNumber+"}";
    	}
    	else if(firstNumber <= fourthNumber && fourthNumber <= secondNumber && secondNumber <= thirdNumber) {
    		
    		return "Sorted:{"+firstNumber+","+fourthNumber+","+secondNumber+","+thirdNumber+"}";
    		
    	}
    	else if(firstNumber <= fourthNumber && fourthNumber <= thirdNumber && thirdNumber <= secondNumber) {
    		
    		return "Sorted:{"+firstNumber+","+fourthNumber+","+thirdNumber+","+secondNumber+"}";
    	}
    	else if (secondNumber <= firstNumber && firstNumber <= thirdNumber && thirdNumber <= fourthNumber) {
    		
    		return "Sorted:{"+secondNumber+","+firstNumber+","+thirdNumber+","+fourthNumber+"}";
    		
    	}
    	else if (secondNumber <= firstNumber && firstNumber <= fourthNumber && fourthNumber <= thirdNumber) {

    		return "Sorted:{"+secondNumber+","+firstNumber+","+fourthNumber+","+thirdNumber+"}";
    		
    	}
    	else if (secondNumber <= thirdNumber && thirdNumber <= fourthNumber && fourthNumber <= firstNumber) {
    		
    		return "Sorted:{"+secondNumber+","+thirdNumber+","+fourthNumber+","+firstNumber+"}";
    	}
    	else if (secondNumber <= thirdNumber && thirdNumber <= firstNumber && firstNumber <= fourthNumber) {
    		
    		return "Sorted:{"+secondNumber+","+thirdNumber+","+firstNumber+","+fourthNumber+"}";
    	}
    	else if (secondNumber <= fourthNumber && fourthNumber <= firstNumber && firstNumber <= thirdNumber) {
    		
    		return "Sorted:{"+secondNumber+","+fourthNumber+","+firstNumber+","+thirdNumber+"}";
    	}
    	else if(secondNumber <= fourthNumber && fourthNumber <= thirdNumber && thirdNumber <= firstNumber) {
    		
    		return "Sorted:{"+secondNumber+","+fourthNumber+","+thirdNumber+","+firstNumber+"}";
    	}
    	else if (thirdNumber <= firstNumber && firstNumber <= secondNumber && secondNumber <= fourthNumber) {
    		
    		return "Sorted:{"+thirdNumber+","+firstNumber+","+secondNumber+","+fourthNumber+"}";
    	}
    	else if (thirdNumber <= firstNumber && firstNumber <= fourthNumber && fourthNumber <= secondNumber) {
    		
    		return "Sorted:{"+thirdNumber+","+firstNumber+","+fourthNumber+","+secondNumber+"}";
    		
    	}
    	else if (thirdNumber <= secondNumber && secondNumber <= firstNumber && firstNumber <= fourthNumber) {
    		
    		return "Sorted:{"+thirdNumber+","+secondNumber+","+firstNumber+","+fourthNumber+"}";
    		
    	}
    	else if (thirdNumber <= secondNumber && secondNumber <= fourthNumber && fourthNumber <= firstNumber) {
    		
    		return "Sorted:{"+thirdNumber+","+secondNumber+","+fourthNumber+","+firstNumber+"}";
    	}
    	else if (thirdNumber <= fourthNumber && fourthNumber <= secondNumber && secondNumber <= firstNumber) {
    		
    		return "Sorted:{"+thirdNumber+","+fourthNumber+","+secondNumber+","+firstNumber+"}";
    	}
    	else if(thirdNumber <= fourthNumber && fourthNumber <= firstNumber && firstNumber <= secondNumber) {
    		
    		return "Sorted:{"+thirdNumber+","+fourthNumber+","+firstNumber+","+secondNumber+"}";
    	}
    	else if (fourthNumber <= firstNumber && firstNumber <= secondNumber && secondNumber <= thirdNumber) {
    		
    		return "Sorted:{"+fourthNumber+","+firstNumber+","+secondNumber+","+thirdNumber+"}";
    	}
    	else if (fourthNumber <= firstNumber && firstNumber <= thirdNumber && thirdNumber <= secondNumber) {
    		
    		return "Sorted:{"+fourthNumber+","+firstNumber+","+thirdNumber+","+secondNumber+"}";
    	}
    	else if (fourthNumber <= secondNumber && secondNumber <= firstNumber && firstNumber <= thirdNumber) {
    		
    		return "Sorted:{"+fourthNumber+","+secondNumber+","+firstNumber+","+thirdNumber+"}";
    	}
    	else if (fourthNumber <= secondNumber && secondNumber <= thirdNumber && thirdNumber <= firstNumber ) {
    		
    		return "Sorted:{"+fourthNumber+","+secondNumber+","+thirdNumber+","+firstNumber+"}";
    	}
    	else if (fourthNumber <= thirdNumber && thirdNumber <= secondNumber && secondNumber <= firstNumber) {
    		
    		return "Sorted:{"+fourthNumber+","+thirdNumber+","+secondNumber+","+firstNumber+"}";
    	}
    	else if (fourthNumber <= thirdNumber && thirdNumber <= firstNumber && firstNumber <= secondNumber) {
    		
    		return "Sorted:{"+fourthNumber+","+thirdNumber+","+firstNumber+","+secondNumber+"}";
    	}*/
    	
       int[] array= {firstNumber,secondNumber,thirdNumber,fourthNumber};
       for (int i= 0;i<=array.length-1;i++) {
    	   for(int j =0;j<array.length;j++) {
    		   if (array[i]<array[j]) {
    			   
    			   int temp =array[i];
    			   array[i]= array[j];
    			   array[j] =temp;
    		   }
    	   }
       }
       
       return "Sorted:{"+array[0]+","+array[1]+","+array[2]+","+array[3]+"}";
        
    }
}
